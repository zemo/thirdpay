package com.zemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @ClassName: PayController
 * @author: SuperZemo
 * @email: zemochen@gmail.com
 * @Date 12/31/15 16:25
 * @Description ${TODO}
 */
@Controller
public class PayController {

    @RequestMapping(value="/", method = RequestMethod.GET)
    public String index() {


        return "index";
    }
    @RequestMapping(value = "/alipay", method = RequestMethod.GET)
    public String question(@PathVariable(value = "id") Long postId, Model model) {


        return "alipay";
    }

}
